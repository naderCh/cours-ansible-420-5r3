Role Name
=========

deploy motd to RedHat based machines

Requirements
------------

no pre-requisties

Role Variables
--------------

system_manager is defined in defaults/main.yml, should be set to admin email

Dependencies
------------

no dependencies

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: motd
           vars:
            system_manager: "nader@test.local"

License
-------

GPL 2.0

Author Information
------------------

Nader Chaabouni - college de Maisonneuve